package com.point.plat.protocol.plugin.codec;

import org.apache.log4j.Logger;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.demux.MessageDecoder;
import org.apache.mina.filter.codec.demux.MessageDecoderResult;
import org.springframework.stereotype.Component;

import com.point.iot.base.tools.CommUtils;
import com.point.iot.base.tools.MessageUtil;
import com.point.plat.protocol.plugin.tcp.model.CjyTcpMessage;
@Component
public class MessageTcpDecoder implements MessageDecoder{
	Logger logger = Logger.getLogger(MessageTcpDecoder.class) ;
	@Override
	public MessageDecoderResult decodable(IoSession session, IoBuffer buf) {
		return MessageDecoderResult.OK;
	}

	@Override
	public MessageDecoderResult decode(IoSession session, IoBuffer buf, ProtocolDecoderOutput out) throws Exception {
		CjyTcpMessage message = new CjyTcpMessage();
		logger.info("收到采集仪消息" + buf.getHexDump());
		message.setCmd(0x00);
		message.setAddress(1234);
		message.setData("aaaaa".getBytes());
		out.write(message);
		return MessageDecoderResult.OK;
	}

	@Override
	public void finishDecode(IoSession session, ProtocolDecoderOutput out) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
